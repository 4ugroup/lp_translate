$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"><i class="fa fa-times"></i></a>',
        wrap     : '<div class="fancybox-wrap fancybox-wrap-wedding" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner wedding-inner"></div></div></div></div>'
    }
});

$('.order-timer').countdown_sg(5);