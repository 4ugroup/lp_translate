var text = new Array();
var time = 0;
$(document).ready(function () {

    $(".fancybox").fancybox({'width': 1350, helpers: {overlay: {locked: false}}});

    // ####### Извлечение ID посетителя #######
    setTimeout(function() {$("figure.effect-dexter").addClass("anim");}, 1000);
    function getUrlVar(){
        var urlVar = window.location.search; // получаем параметры из урла
        var arrayVar = []; // массив для хранения переменных
        var valueAndKey = []; // массив для временного хранения значения и имени переменной
        var resultArray = []; // массив для хранения переменных
        arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
        if(arrayVar[0]=="") return false; // если нет переменных в урле
        for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
            valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
            resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
        }
        return resultArray; // возвращаем результат
    }
    var id_p = getUrlVar();
    // Проверка: указаy ли ID
    $("#id_p").val(id_p["id"]);

    // ####### Запись времени захода #######
    var date = new Date();
    var prov = 0;
    $("#id_time").val(id_p["id"]);
    $("#time").val(date.getTime());
    $.ajax({
        type: 'POST',
        url: 'time.php',
        data: jQuery("#timeForm").serialize(),
        success: function(response){
            prov = response;
            if (prov != "no") {
                window.text = prov.split('||');
                window.time = parseInt(window.text[1]) + 172800000;
                if (date.getTime() < window.time) {
                    ringer.init();
                } else {
                    $(".timer .left span.r").html("для вас больше не актуальна!");
                    $(".timer .left span.r").css("color", "#f00");
                }

                // ####### Появление тела #######
                $(".telo-3").css("display", "block");
                $(".input_name").addClass("input--filled");
                $("#name").val(window.text[0]);
                setTimeout(function() {
                    $(".timer .left").addClass("anim");
                    $("canvas").addClass("anim");
                    $(".form").addClass("anim");
                    $(".video").addClass("anim");
                    //video_pre.play();
                }, 1000);
            } else {
                $(".telo-3").css("display", "block");
                setTimeout(function() {
                    $(".form").html("");
                    $(".video").html("");
                    //$(".rezult").html("<h2>Ссылка на видео отправлена</h2> на указанный вами E-mail.<br> Открывайте! Будет интересно!"+response);
                    $(".rezult").html("<h2>Некорректная ссылка</h2> для получение доступа к видео пожалуйста зарегистрируйтесь <a href='/info/'>здесь &gt;&gt;</a>");
                    $(".rezult").addClass("anim");
                }, 1000);
            }
        }
    });
});


/* скрипт полей формы */
(function() {
    // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
    if (!String.prototype.trim) {
        (function() {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
                return this.replace(rtrim, '');
            };
        })();
    }

    [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
        // in case the input is already filled..
        if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'input--filled' );
        }

        // events:
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );

    function onInputFocus( ev ) {
        classie.add( ev.target.parentNode, 'input--filled' );
    }

    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'input--filled' );
        }
    }
})();

/* ---- Отправка формы ---- */
function isName(name) {
    // регулярное вырожение для проверки введенного bvtyb
    var regex_name = /^[а-яА-ЯёЁa-zA-Z0-9\s]+$/;
    return regex_name.test(name);
}

function isPhone(phone) {
    // регулярное вырожение для проверки введенного адреса
    var regex = /^[0-9\-\_\+\(\)\s]+$/;
    return regex.test(phone);
}

function completeInviteForm() {

    $(".form").removeClass("anim");
    $(".video").removeClass("anim");
    $(".timer .left").removeClass("anim");
    $("canvas").removeClass("anim");

    $.ajax({
        type: 'POST',
        url: 'save2.php',
        data: jQuery("#inviteform").serialize(),
        success: function(response){
            //$('#completeform').before('<span class="msg">Все готово! Вы добавлены в список рассылки.</span>');
            setTimeout(function(){
                $(".form").css("display", "none");
                $(".video").css("display", "none");
                $(".timer").css("display", "none");
                $("canvas").css("display", "none");
                $(".rezult").css("display", "block");
                $(".rezult").addClass("anim");
                //if (response == 0) {
                $(".rezult").html("<h2>Спасибо за Ваш заказ!</h2>В самое ближайшее время с вами<br>свяжется наш менеджер.");
                yaCounter31798636.reachGoal('ZAKAZ');
                /*}
                 if (response == 1) {
                 $(".rezult").html("При отправке произошла ошибка");
                 }
                 if (response == 2) {
                 $(".rezult").html('<h2>Такой E-mail уже зарегистрирован!</h2> Ссылка уже должна быть у вас на почте. Если Вы не можете найти письмо, проверьте папку "Спам". Если не обнаружили письмо и там, позвоните пожалуйста нам: info@4u-group.ru.');
                 }*/
            }, 1500);
        }
    });
}

var erdiv    = $("#error");
var btnwrap  = $("#btnwrap");


$("#sendbtn").click(function(e){
    // После нажатия на кнопку отменяем стандартное поведение браузера
    // и создаем переменную
    e.preventDefault();
    var nameval = $("#name").val();
    var phoneval = $("#phone").val();



    if(!isName(nameval)) {
        erdiv.html("Вы ввели некорректное имя");
        erdiv.css("height", "20px");
        $(".input_name").addClass("error");
    } else {
        $(".input_name").removeClass("error");
    }

    if(!isPhone(phoneval)) {
        erdiv.html("Вы неправильно ввели номер телефона");
        erdiv.css("height", "20px");
        $(".input_phone").addClass("error");
    } else {
        $(".input_phone").removeClass("error");
    }

    if((!isPhone(phoneval))&&(!isName(nameval))) {
        erdiv.html("Вы неправильно ввели имя и номер телефона");
        erdiv.css("height", "20px");
    }

    if((isPhone(phoneval))&&(isName(nameval))) {
        erdiv.css("height", "0px");
        /*erdiv.css("color", "#719dc8");
         erdiv.html("Отправка");
         btnwrap.html('<img src="img/loader.gif" alt="loading">');*/
        completeInviteForm();
    }
});



/* ############ Счетчик обратного отсчета ########### */
var ringer = {
    //countdown_to: "10/31/2014",
    countdown_to: "10/31/2016",
    rings: {
        'ДЕНЬ': {
            s: 86400000, // mseconds in a day,
            max: 365
        },
        'ЧАСОВ': {
            s: 3600000, // mseconds per hour,
            max: 24
        },
        'МИНУТ': {
            s: 60000, // mseconds per minute
            max: 60
        },
        'СЕКУНД': {
            s: 1000,
            max: 60
        }
    },
    r_count: 4,
    r_spacing: 10, // px
    r_size: 80, // px
    r_thickness: 2, // px
    update_interval: 11, // ms


    init: function(){

        $r = ringer;
        $r.cvs = document.createElement('canvas');

        $r.size = {
            w: ($r.r_size + $r.r_thickness) * $r.r_count + ($r.r_spacing*($r.r_count-1)),
            h: ($r.r_size + $r.r_thickness)
        };



        $r.cvs.setAttribute('width',$r.size.w);
        $r.cvs.setAttribute('height',$r.size.h);
        $r.ctx = $r.cvs.getContext('2d');
        $(document.body).append($r.cvs);
        $r.cvs = $($r.cvs);
        $r.ctx.textAlign = 'center';
        $r.actual_size = $r.r_size + $r.r_thickness;
        $r.countdown_to_time = new Date(window.time).getTime();
        $r.cvs.css({ width: $r.size.w+"px", height: $r.size.h+"px" });
        $r.go();
    },
    ctx: null,
    go: function(){
        var idx=0;
        var prt=new Date().getTime();

        $r.time = prt - $r.countdown_to_time;
        prt = prt - $r.countdown_to_time;

        for(var r_key in $r.rings) $r.unit(idx++,r_key,$r.rings[r_key]);

        if(prt < -1000) {
            setTimeout($r.go,$r.update_interval);
        } else {
            $("canvas").removeClass("anim");
            $(".timer .left span.r").html("для вас больше не актуальна!");
            $(".timer .left span.r").css("color", "#f00");
        }
    },
    unit: function(idx,label,ring) {
        var x,y, value, ring_secs = ring.s;
        value = parseFloat($r.time/ring_secs);
        $r.time-=Math.round(parseInt(value)) * ring_secs;
        value = Math.abs(value);

        x = ($r.r_size*.5 + $r.r_thickness*.5);
        x +=+(idx*($r.r_size+$r.r_spacing+$r.r_thickness));
        y = $r.r_size*.5;
        y += $r.r_thickness*.5;


        // calculate arc end angle
        var degrees = 360-(value / ring.max) * 360.0;
        var endAngle = degrees * (Math.PI / 180);

        $r.ctx.save();

        $r.ctx.translate(x,y);
        $r.ctx.clearRect($r.actual_size*-0.5,$r.actual_size*-0.5,$r.actual_size,$r.actual_size);

        // first circle
        $r.ctx.strokeStyle = "rgba(128,128,128,0.2)";
        $r.ctx.beginPath();
        $r.ctx.arc(0,0,$r.r_size/2,0,2 * Math.PI, 2);
        $r.ctx.lineWidth =$r.r_thickness;
        $r.ctx.stroke();

        // second circle
        $r.ctx.strokeStyle = "rgba(253, 128, 1, 0.9)";
        $r.ctx.beginPath();
        $r.ctx.arc(0,0,$r.r_size/2,0,endAngle, 1);
        $r.ctx.lineWidth =$r.r_thickness;
        $r.ctx.stroke();

        // label
        $r.ctx.fillStyle = "#ffffff";

        $r.ctx.font = '10px Helvetica';
        $r.ctx.fillText(label, 0, 23);
        $r.ctx.fillText(label, 0, 23);

        $r.ctx.font = 'bold 35px Helvetica';
        $r.ctx.fillText(Math.floor(value), 0, 10);

        $r.ctx.restore();
    }
}

/* ################### Видео ################# */

var status_video = 0;
var status_port = 0;
$(".video").mouseover(function(){$("#play_stop").css("opacity", "1");});
$(".video").mouseout(function(){
    if (status_video == 0) {
        $("#play_stop").css("opacity", "0.6");
    } else {
        $("#play_stop").css("opacity", "0");
    }
});

$(".video").click(function(){
    $("#play_stop").toggleClass("pause");
    if (status_video == 0) {
        video_pre.play();
        status_video = 1;
        $(".video.osnov").addClass("polno");
        // -------------- Подгрузка портфолио --------------
        setTimeout(function(){
            if (status_port == 0) {
                status_port = 1;
                $.ajax({
                    type: 'GET',
                    url: 'port.html',
                    success: function(response){
                        $(".portfolio").html(response);
                    }
                });
            }
        }, 60000);
    } else {
        video_pre.pause();
        status_video = 0;
        $(".video.osnov").removeClass("polno");
    }
});


var port_stoper = setInterval(function(){
    if (video_pre.currentTime > 3192) {
        video_pre.pause();
        status_video = 0;
        $(".video.osnov").removeClass("polno");
        clearInterval(port_stoper);
        setTimeout(function(){
            $(".portfolio").css("height", "5000px");

            var target = ".slider-item.item1",
                $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                window.location.hash = target;
            });

        }, 1000);
    }
}, 1000);





// Показ слайдов при наведении
//============================================Обслуживание
// 1

$(document).on("mouseover", ".slider-item.item1 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item11');
    console.log(check);
    $(".slider-item.item1 .kamena").removeClass("kamena");
    $(".slider-item.item1 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    e.preventDefault();
    var target = ".slider-item.item1",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item1 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item12');
    console.log(check);
    $(".slider-item.item1 .kamena").removeClass("kamena");
    $(".slider-item.item1 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item1",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item1 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item13');
    console.log(check);
    $(".slider-item.item1 .kamena").removeClass("kamena");
    $(".slider-item.item1 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item1",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item1 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item14');
    console.log(check);
    $(".slider-item.item1 .kamena").removeClass("kamena");
    $(".slider-item.item1 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item1",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
//=========================================================
//================================================== икра
$(document).on("mouseover", ".slider-item.item4 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item21');
    console.log(check);
    $(".slider-item.item4 .kamena").removeClass("kamena");
    $(".slider-item.item4 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item4",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item4 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item22');
    console.log(check);
    $(".slider-item.item4 .kamena").removeClass("kamena");
    $(".slider-item.item4 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item4",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item4 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item23');
    console.log(check);
    video_2.play();
    $(".slider-item.item4 .kamena").removeClass("kamena");
    $(".slider-item.item4 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item4",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item4 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item24');
    console.log(check);
    $(".slider-item.item4 .kamena").removeClass("kamena");
    $(".slider-item.item4 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item4",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
//==================================================
//================================================== кристалл
$(document).on("mouseover", ".slider-item.item2 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item51');
    console.log(check);
    $(".slider-item.item2 .kamena").removeClass("kamena");
    $(".slider-item.item2 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item2",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item2 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item52');
    console.log(check);
    $(".slider-item.item2 .kamena").removeClass("kamena");
    $(".slider-item.item2 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item2",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item2 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item53');
    console.log(check);
    video_5.play();
    $(".slider-item.item2 .kamena").removeClass("kamena");
    $(".slider-item.item2 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item2",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item2 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item54');
    console.log(check);
    $(".slider-item.item2 .kamena").removeClass("kamena");
    $(".slider-item.item2 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item2",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
//==================================================
//================================================== Банк icb
$(document).on("mouseover", ".slider-item.item3 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item61');
    console.log(check);
    $(".slider-item.item3 .kamena").removeClass("kamena");
    $(".slider-item.item3 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item3",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item3 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item62');
    console.log(check);
    video_6.play();
    $(".slider-item.item3 .kamena").removeClass("kamena");
    $(".slider-item.item3 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item3",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item3 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item63');
    console.log(check);
    $(".slider-item.item3 .kamena").removeClass("kamena");
    $(".slider-item.item3 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item3",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item3 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item64');
    console.log(check);
    $(".slider-item.item3 .kamena").removeClass("kamena");
    $(".slider-item.item3 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item3",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
// 5
$(document).on("mouseover", ".slider-item.item3 .skills li.item5", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item65');
    console.log(check);
    video.play();
    $(".slider-item.item3 .kamena").removeClass("kamena");
    $(".slider-item.item3 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item3",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});
// 5
//==================================================
//================================================== Монтаж
$(document).on("mouseover", ".slider-item.item6 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item31');
    console.log(check);
    $(".slider-item.item6 .kamena").removeClass("kamena");
    $(".slider-item.item6 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item6",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item6 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item32');
    console.log(check);
    $(".slider-item.item6 .kamena").removeClass("kamena");
    $(".slider-item.item6 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item6",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item6 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item33');
    console.log(check);
    video_3.play();
    $(".slider-item.item6 .kamena").removeClass("kamena");
    $(".slider-item.item6 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item6",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item6 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item34');
    console.log(check);
    $(".slider-item.item6 .kamena").removeClass("kamena");
    $(".slider-item.item6 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item6",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
//==================================================
//================================================== icbPartners
$(document).on("mouseover", ".slider-item.item5 .skills li.item1", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item41');
    console.log(check);
    $(".slider-item.item5 .kamena").removeClass("kamena");
    $(".slider-item.item5 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item5",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 2

$(document).on("mouseover", ".slider-item.item5 .skills li.item2", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item42');
    console.log(check);
    video_4.play();
    $(".slider-item.item5 .kamena").removeClass("kamena");
    $(".slider-item.item5 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item5",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});


// 3

$(document).on("mouseover", ".slider-item.item5 .skills li.item3", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item43');
    console.log(check);
    $(".slider-item.item5 .kamena").removeClass("kamena");
    $(".slider-item.item5 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item5",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4

$(document).on("mouseover", ".slider-item.item5 .skills li.item4", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item44');
    console.log(check);
    $(".slider-item.item5 .kamena").removeClass("kamena");
    $(".slider-item.item5 .ruka").css("display", "none");
    check.fadeIn("fast");
    $('.portfolio-nav').fadeOut("fast");
    $('.slider-view').fadeOut("fast");

    var target = ".slider-item.item5",
        $target = $(target);
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });

});

// 4
//==================================================

/*$('.skills li.item5').mouseover(function() {
 var box = $(this).closest('.slider-item');
 var check = box.find('.portfolio-item05');
 console.log(check);
 check.fadeIn("fast");
 $('.portfolio-nav').fadeOut("fast");
 $('.slider-view').fadeOut("fast");
 });*/

$(document).on("mouseout", ".skills li", function() {
    var box = $(this).closest('.slider-item');
    var check = box.find('.portfolio-item');
    check.fadeOut("fast");
    video.pause();
    video_2.pause();
    video_3.pause();
    video_4.pause();
    video_5.pause();
    video_6.pause();
    $('.portfolio-nav').fadeIn("fast");
    $('.slider-view').fadeIn("fast");
});
/**
 * Created by zephy on 15.03.2016.
 */
